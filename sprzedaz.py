import sys

from helper import read_from_file, drop_file_data
from main import perform_actions_from_file, perform_sprzedaz

args_from_call = sys.argv[2:]
print(args_from_call)

drop_file_data('output.txt')
lines_from_file = read_from_file('input.txt')
perform_actions_from_file(lines_from_file)

print("Wybrano tryb sprzedaż.")
sale_product_ID = args_from_call[0]
sale_unit_price = int(args_from_call[1])
sale_nr_items = int(args_from_call[2])
perform_sprzedaz(sale_product_ID, sale_unit_price, sale_nr_items)
