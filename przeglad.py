from helper import write_to_file, read_from_file
import sys
from main import perform_actions_from_file
import main

args_from_call = sys.argv[1:]

lines_from_file = read_from_file('input.txt')
perform_actions_from_file(lines_from_file)

print("Wybrano tryb przeglad.")
description = f"Przeglad operacji"
main.logs.append(['przeglad', description])
write_to_file('output.txt', str(main.logs[-1]))
