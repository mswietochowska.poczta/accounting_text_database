from helper import read_from_file, drop_file_data
import sys
from main import perform_actions_from_file, perform_saldo

args_from_call = sys.argv[1:]

drop_file_data('output.txt')
lines_from_file = read_from_file('input.txt')
perform_actions_from_file(lines_from_file)


print("Wybrano tryb saldo.")
change_in_account = int(args_from_call[1])
description = args_from_call[2]
perform_saldo(change_in_account, description)
