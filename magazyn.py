from helper import write_to_file, read_from_file
import sys
from main import perform_actions_from_file
import main

args_from_call = sys.argv[2:]

lines_from_file = read_from_file('input.txt')
perform_actions_from_file(lines_from_file)

print("Wybrales tryb magazyn.")
id_prod = args_from_call[0]


def sprawdz_stan_magazynowy(id_product):
    description = f"Sprawdzenie stanu magazynowego dla {id_product}"
    if id_product not in main.warehouse:
        print("Brak przedmiotu w magazynie.")
        stan = "BRAK"
    else:
        stan = main.warehouse[id_product]

    main.logs.append(['magazyn', id_product, stan, description])
    write_to_file('output.txt', str(main.logs[-1]))


sprawdz_stan_magazynowy(id_prod)
