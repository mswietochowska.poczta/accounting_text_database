def read_from_file(file_name):
    with open(file_name, 'r') as file:
        result = []
        for line in file:
            result.append(line.rstrip())
        return result


def write_to_file(file_name, data):
    with open(file_name, 'a') as file:
        file.write(f"{data}\n")


def drop_file_data(file_name):
    with open(file_name, 'w') as f:
        f.truncate()
