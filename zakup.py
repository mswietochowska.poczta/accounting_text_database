from helper import read_from_file, drop_file_data
import sys
from main import perform_actions_from_file, perform_zakup

args_from_call = sys.argv[2:]

drop_file_data('output.txt')
lines_from_file = read_from_file('input.txt')
perform_actions_from_file(lines_from_file)

print("Wybrano tryb zakupu.")
purchase_product_ID = args_from_call[0]
purchase_until_price = int(args_from_call[1])
purchase_nr = int(args_from_call[2])

perform_zakup(purchase_product_ID, purchase_until_price, purchase_nr)
