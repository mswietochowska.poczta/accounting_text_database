from helper import write_to_file

balance = 1000
warehouse = {}
logs = []


def perform_saldo(value, description):
    global balance, warehouse, logs
    new_balance = balance + value
    if new_balance < 0:
        print("Nie można wykonać operacji. Stan konta nie może być ujemny.")
        return
    balance = new_balance
    logs.append(['saldo', value, description])
    write_to_file('output.txt', logs[-1])


def perform_zakup(purchase_product_ID, purchase_until_price, purchase_nr_items):
    global balance, warehouse, logs

    if purchase_until_price * purchase_nr_items > balance:
        print("Nie wystarczająca ilość środków na koncie.")
        return

    if purchase_product_ID in warehouse:

        warehouse[purchase_product_ID] += purchase_nr_items
    else:
        warehouse[purchase_product_ID] = purchase_nr_items
    balance -= purchase_until_price * purchase_nr_items
    logs.append(['zakup', warehouse, balance])
    write_to_file('output.txt', logs[-1])


def perform_sprzedaz(sale_product_ID, sale_unit_price, sale_nr_items):
    global balance, warehouse, logs

    if sale_unit_price < 0:
        print("Blad. Cena nie moze byc ujemna")
        return

    if sale_product_ID in warehouse:
        if warehouse[sale_product_ID] - sale_nr_items < 0:
            print("Nie wystarczająca ilośc przedmiotu w magazynie.")
            return
        else:
            warehouse[sale_product_ID] -= sale_nr_items
    else:
        print('Brak towaru w magazynie. Nie można wykonać sprzedaży.')
        return

    balance += sale_unit_price * sale_nr_items
    logs.append(['sprzedaz', warehouse, balance])
    write_to_file('output.txt', logs[-1])


def perform_actions_from_file(lines):
    global logs
    for i, line in enumerate(lines):
        if line == "saldo":
            perform_saldo(int(lines[i + 1]), lines[i + 2])

        elif line == "zakup":
            perform_zakup(lines[i + 1], int(lines[i + 2]), int(lines[i + 3]))

        elif line == "sprzedaz":
            perform_sprzedaz(lines[i + 1], int(lines[i + 2]), int(lines[i + 3]))
